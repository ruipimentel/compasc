#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Imports para ambiente gráfico:
import wx
import gettext
from CompascMainWindow_functions	import CompascMainWindow
from CompascRecordWindow_functions	import CompascRecordWindow
from CompascAboutWindow_functions	import CompascAboutWindow

if __name__ == "__main__":
	gettext.install("app") # replace with the appropriate catalog name

	app = wx.PySimpleApp(0)
	wx.InitAllImageHandlers()
	window_main = CompascMainWindow(None, wx.ID_ANY, "")
	app.SetTopWindow(window_main)
	window_main.Show()
	app.MainLoop()
