#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from CompascAboutWindow import CompascAboutWindow

class CompascAboutWindow(CompascAboutWindow):
	def button_ok_click_handler(self, event):  # wxGlade: CompascAboutWindow.<event_handler>
		self.Close()
