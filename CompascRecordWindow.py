# -*- coding: UTF-8 -*-
#
# generated by wxGlade 0.6.8 on Tue Aug 30 09:23:08 2016
#

import wx
# begin wxGlade: dependencies
# end wxGlade

# begin wxGlade: extracode
import matplotlib
matplotlib.use('WX')
from matplotlib.backends.backend_wx import FigureCanvasWx
from matplotlib.figure import Figure
# end wxGlade


class CompascRecordWindow(wx.Dialog):
	def __init__(self, *args, **kwds):
		# begin wxGlade: CompascRecordWindow.__init__
		kwds["style"] = wx.DEFAULT_DIALOG_STYLE
		wx.Dialog.__init__(self, *args, **kwds)
		self.canvas = FigureCanvasWx(self, wx.ID_ANY, Figure())
		self.button_gravar = wx.lib.buttons.GenBitmapToggleButton(self, wx.ID_ANY)
		self.button_pausar = wx.lib.buttons.GenBitmapButton(self, wx.ID_ANY)
		self.button_mutar = wx.lib.buttons.GenBitmapToggleButton(self, wx.ID_ANY)
		self.label_1 = wx.StaticText(self, wx.ID_ANY, _("Volume:"))
		self.slider_volume = wx.Slider(self, wx.ID_ANY, 100, 0, 100, style=wx.SL_HORIZONTAL | wx.SL_LEFT | wx.SL_RIGHT)
		self.listctrl_gravacoes = wx.ListCtrl(self, wx.ID_ANY, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
		self.button_analisar = wx.Button(self, wx.ID_ANY, _("Analisar"))
		self.button_excluir = wx.Button(self, wx.ID_ANY, _("Excluir"))
		self.button_fechar = wx.Button(self, wx.ID_ANY, _("Fechar"))

		self.__set_properties()
		self.__do_layout()

		self.Bind(wx.EVT_BUTTON, self.button_fechar_click_handler, self.button_fechar)
		# end wxGlade

	def __set_properties(self):
		# begin wxGlade: CompascRecordWindow.__set_properties
		self.SetTitle(_(u"Gravador de \xe1udio"))
		self.SetSize((680, 700))
		self.canvas.SetMinSize((-1, 300))
		self.button_gravar.SetToolTipString(_(u"Clique para iniciar a grava\xe7\xe3o de \xe1udio."))
		self.button_gravar.SetFocus()
		self.button_gravar.SetBitmapLabel(wx.Bitmap("../material-design-icons-2.2.0/av/1x_web/ic_fiber_manual_record_black_24dp.png", wx.BITMAP_TYPE_ANY))
		self.button_gravar.SetBitmapSelected(wx.Bitmap("../material-design-icons-2.2.0/av/1x_web/ic_stop_black_24dp.png", wx.BITMAP_TYPE_ANY))
		self.button_pausar.SetToolTipString(_(u"Clique para pausar a grava\xe7\xe3o de \xe1udio."))
		self.button_pausar.Enable(False)
		self.button_pausar.SetBitmapLabel(wx.Bitmap("../material-design-icons-2.2.0/av/1x_web/ic_pause_black_24dp.png", wx.BITMAP_TYPE_ANY))
		self.button_mutar.SetToolTipString(_(u"Clique para tornar muda a reprodu\xe7\xe3o de \xe1udio."))
		self.button_mutar.SetBitmapLabel(wx.Bitmap("../material-design-icons-2.2.0/av/1x_web/ic_volume_off_black_24dp.png", wx.BITMAP_TYPE_ANY))
		self.button_mutar.SetBitmapSelected(wx.Bitmap("../material-design-icons-2.2.0/av/1x_web/ic_volume_up_black_24dp.png", wx.BITMAP_TYPE_ANY))
		self.slider_volume.SetMinSize((100, 18))
		self.listctrl_gravacoes.SetMinSize((-1, 150))
		# end wxGlade

	def __do_layout(self):
		# begin wxGlade: CompascRecordWindow.__do_layout
		sizer_9 = wx.BoxSizer(wx.VERTICAL)
		sizer_13 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_14 = wx.BoxSizer(wx.VERTICAL)
		sizer_11 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_12 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_10 = wx.BoxSizer(wx.HORIZONTAL)
		sizer_10.Add(self.canvas, 1, wx.EXPAND, 0)
		sizer_9.Add(sizer_10, 1, wx.ALL | wx.EXPAND, 25)
		sizer_11.Add(self.button_gravar, 1, wx.EXPAND, 0)
		sizer_11.Add(self.button_pausar, 1, wx.EXPAND, 0)
		sizer_11.Add(self.button_mutar, 1, wx.EXPAND, 0)
		sizer_12.Add(self.label_1, 0, 0, 0)
		sizer_12.Add(self.slider_volume, 0, 0, 0)
		sizer_11.Add(sizer_12, 0, wx.LEFT | wx.ALIGN_CENTER_VERTICAL, 25)
		sizer_9.Add(sizer_11, 0, wx.LEFT | wx.RIGHT | wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL, 25)
		sizer_13.Add(self.listctrl_gravacoes, 1, wx.RIGHT | wx.EXPAND, 25)
		sizer_14.Add(self.button_analisar, 0, 0, 0)
		sizer_14.Add(self.button_excluir, 0, 0, 0)
		sizer_13.Add(sizer_14, 0, wx.ALIGN_CENTER_VERTICAL, 0)
		sizer_9.Add(sizer_13, 0, wx.LEFT | wx.RIGHT | wx.BOTTOM | wx.EXPAND, 25)
		sizer_9.Add(self.button_fechar, 0, wx.LEFT | wx.RIGHT | wx.BOTTOM | wx.ALIGN_CENTER_HORIZONTAL, 25)
		self.SetSizer(sizer_9)
		self.Layout()
		# end wxGlade

	def button_fechar_click_handler(self, event):  # wxGlade: CompascRecordWindow.<event_handler>
		print "Event handler 'button_fechar_click_handler' not implemented!"
		event.Skip()

# end of class CompascRecordWindow
