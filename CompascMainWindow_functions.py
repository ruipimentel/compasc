#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from CompascMainWindow				import CompascMainWindow
from CompascRecordWindow_functions	import CompascRecordWindow
from CompascAboutWindow_functions	import CompascAboutWindow

class CompascMainWindow(CompascMainWindow):
	def menu_ferramentas_gravador_de_audio_click_handler(self, event):  # wxGlade: CompascMainWindow.<event_handler>
		w = CompascRecordWindow(self)
		w.ShowModal()
		w.Destroy()

	def menu_ajuda_sobre_click_handler(self, event):  # wxGlade: CompascMainWindow.<event_handler>
		w = CompascAboutWindow(self)
		w.ShowModal()
		w.Destroy()
	
	def OpenRecord(self, chunks, cabecalho=None):
		print "Recebeu {} chunks de {} bytes".format(len(chunks), len(chunks[0]))
