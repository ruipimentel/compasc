#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from CompascRecordWindow import CompascRecordWindow

# Imports da reprodução de áudio:
import pyaudio
import struct

# Imports da gravação de áudio:
import wave

# Imports e constantes da comunicação serial:
import serial
import math
import numpy as np
from collections import deque	# Estrutura de dados para armazenamento das amostras lidas.

# Imports gerais
import wx
import wx.lib.buttons as buttons
import time
import threading

# Parâmetros gerais
BYTES_PER_FRAME = 2
CHANNELS = 1
INTENDED_RATE = 11025
SAMPLING_RATE = 11111
PLAYBACK_RATE = 11100
PLAYBACK_FRAMES_PER_BUFFER = 1024
GAIN = 16
VISIBLE_TIME = 4.0
BUFFERING_TIME = 1.0
SAMPLE_AMOUNT = int(INTENDED_RATE*VISIBLE_TIME)
BUFFER_REFERENCE = SAMPLE_AMOUNT - int(INTENDED_RATE*BUFFERING_TIME)

ser = None
buffer_counter = None
samples = None
audio_chunks = None
t = range(SAMPLE_AMOUNT)

def chunkString(string, length):
	string_length = len(string)/length*length
	return (list(string[0+i:length+i] for i in range(0, string_length, length)), string[string_length:len(string)])

last_read_chunk = None
def callback(in_data, frame_count, time_info, status):
	global last_read_chunk
	
	# Caso não haja amostras suficientes, encerra o playback:
	if len(audio_chunks) == 0:
		return ("", pyaudio.paComplete)
	else:
		last_read_chunk = audio_chunks.popleft()	# Não pode ser variável local, pois temos que evitar o garbage collector.
		return (last_read_chunk, pyaudio.paContinue)

def dataFetcherFunction(wr):
	global buffer_counter, SAMPLE_AMOUNT
	
	# Começa pedindo uma atualização na plotagem:
	event = FrameFetchedEvent()
	event.eventLock = wr.eventLock
	wr.AddPendingEvent(event)
	
	time.sleep(0.2)	# TO DO 2DO TODO: usar a resourceLock para só deixar os eventos Fetch começarem após inicialização da janela.
	
	# Obtém a trava dos recursos de hardware:
	wr.resourceLock.acquire()
	
	try:
		# Inicializa o recurso do serial:
		ser = serial.Serial("/dev/ttyACM0", 115200)
		ser.flush()
		ser.flushInput()
		
		# Inicializa os recursos de áudio:
		p = pyaudio.PyAudio()
		stream = p.open	( format = p.get_format_from_width(BYTES_PER_FRAME)
						, channels = CHANNELS
						, rate = SAMPLING_RATE
						, output = True
						, stream_callback = callback
						, frames_per_buffer = PLAYBACK_FRAMES_PER_BUFFER )
		
		frame_amount_total = 0
		chunk_leftover = ""
	except:
		wr.keep_open = False
	
	while wr.keep_open:
		data_amount = ser.inWaiting()
		data_amount = data_amount & ~1	# para sempre ler uma quantidade par.
		if data_amount > 0:
			data = ser.read(data_amount)
			frame_amount = len(data)/BYTES_PER_FRAME
			samples_read = np.fromstring(data, dtype=np.int16)
			frame_amount_total += frame_amount
			
			# Obtém a trava dos recursos críticos:
			wr.eventLock.acquire()
			
			# Faz as devidas alterações nos recursos críticos:
			for i in range(frame_amount):
				samples_read[i] = GAIN*math.floor(wr.volume*(samples_read[i] - 2048))
				samples.popleft()
				samples.append(samples_read[i])
				chunk_leftover += struct.pack("<h", samples_read[i])
			new_chunks, chunk_leftover = chunkString(chunk_leftover, PLAYBACK_FRAMES_PER_BUFFER*BYTES_PER_FRAME)
			audio_chunks.extend(new_chunks)
			buffer_counter = SAMPLE_AMOUNT - len(audio_chunks)*PLAYBACK_FRAMES_PER_BUFFER
			if wr.is_recording:
				wr.recorded_chunks.extend(new_chunks)
			
			
			# Libera a trava dos recursos críticos:
			wr.eventLock.release()
		
			if frame_amount_total > 1000:
				frame_amount_total = 0
				# Notifica a janela de gravação sobre os novos dados através de um evento:
				event = FrameFetchedEvent()
				event.eventLock = wr.eventLock
				wr.AddPendingEvent(event)
			
			time.sleep(0.05)
			
	try:
		ser.close()
		p.terminate()
		stream.close()
	except:
		pass
	
	# Libera a trava dos recursos de hardware, permitindo à janela fechar:
	wr.resourceLock.release()
	print "Fim da dataFetcherFunction."

class CompascRecordWindow(CompascRecordWindow):
	def __init__(self, *args, **kwds):
		global t, samples, buffer_counter, audio_chunks, BUFFER_REFERENCE, GAIN, SAMPLE_AMOUNT
		super(CompascRecordWindow, self).__init__(*args, **kwds)
		
		# Inicializa as variáveis de reprodução de áudio:
		empty_chunk = struct.pack("<h", 0)*PLAYBACK_FRAMES_PER_BUFFER
		audio_chunks = deque()
		audio_chunks.extend([empty_chunk]*((SAMPLE_AMOUNT - BUFFER_REFERENCE)/PLAYBACK_FRAMES_PER_BUFFER))
		buffer_counter = SAMPLE_AMOUNT - len(audio_chunks)*PLAYBACK_FRAMES_PER_BUFFER
		self.volume = float(self.slider_volume.GetValue())/float(self.slider_volume.GetMax());
		
		# Inicializa os componentes gráficos da plotagem:
		samples = deque([0] * SAMPLE_AMOUNT)
		self.figure = self.canvas.figure
		self.axes1 = self.figure.add_subplot(111)
		self.axes1.set_xlim(0, SAMPLE_AMOUNT)
		self.axes1.set_ylim(-32768, 32767)
		try:
			self.a1, = self.axes1.plot(t, samples)
		except:
			print "\n====ERRO FATAL============================\n\t\tsamples\n{}\t{}".format(
				len(t),
				len(samples)
			)
		self.buffer_line = self.axes1.axvline(color="y")
		self.reference_line = self.axes1.axvline(BUFFER_REFERENCE, color="k")
		
		# Inicia a thread de animação:
		try:
			self.keep_open = True
			self.eventLock = threading.Lock()
			self.resourceLock = threading.Lock()
			thread_fetcher = threading.Thread(target=dataFetcherFunction, args=(self,))
			thread_fetcher.setDaemon(True)
			thread_fetcher.start()
		except:
			print "Não foi possível iniciar a plotagem (thread)."
		
		# Inicializa os objetos de gravação de áudio:
		self.recorded_chunks = []
		self.is_recording = False
		
		# Inicializa os componentes de armazenamento de áudios gravados:
		self.records = []
		self.listctrl_gravacoes.InsertColumn(0, "Data/hora", width=150)
		self.listctrl_gravacoes.InsertColumn(1, "Duração", width=80)
		self.listctrl_gravacoes.InsertColumn(2, "Tamanho", width=100)
		
		# Associa o evento de recuperação de dados com o update da plotagem:
		self.Bind(EVT_FRAME_FETCHED, self.OnFetch)
		self.Bind(wx.EVT_CLOSE, self.OnClose)
		
		# Associa os controles de gravação (gravar, parar, pausar, mute on/off, volume) com seus respectivos eventos:
		self.Bind(wx.EVT_BUTTON, self.button_gravar_toggle_handler, self.button_gravar)
		self.Bind(wx.EVT_BUTTON, self.button_pausar_click_handler, self.button_pausar)
		self.Bind(wx.EVT_BUTTON, self.button_mutar_toggle_handler, self.button_mutar)
		self.Bind(wx.EVT_SLIDER, self.slider_volume_slide_handler, self.slider_volume)
		
		# Associa os controles de manipulação de áudios gravados com seus respectivos eventos:
		self.Bind(wx.EVT_BUTTON, self.button_analisar_click_handler, self.button_analisar)
		self.Bind(wx.EVT_BUTTON, self.button_excluir_click_handler, self.button_excluir)
		
		# Finaliza a inicialização da janela:
		super(CompascRecordWindow, self).__set_properties()
		self.__do_layout(True)
	def OnFetch(self, event):
		global t, samples, buffer_counter
		if event.eventLock.acquire(False):
			self.a1.set_ydata(samples)
			self.buffer_line.set_xdata(buffer_counter)
			try:
				self.figure.canvas.draw()
			except:
				print "\n====ERRO FATAL============================\nTamanhos dos arrays envolvidos na plotagem:\nt\tsamples\n{}\t{}".format(
					len(t),
					len(samples)
				)
				#exit()
				self.Close()
			finally:
				event.eventLock.release()
	def __do_layout(self, override=False):
		if not override:
			super(CompascRecordWindow, self).__do_layout()
		else:
			#self.Sizer.Children[0].Sizer.Add(self.canvas, 0, 0, 0)
			#self.Sizer.Fit(self)
			#self.Layout()
			pass
	def button_fechar_click_handler(self, event):
		self.Close()
	def button_gravar_toggle_handler(self, event):
		if event.GetIsDown():
			self.is_recording = True
			self.button_pausar.Enable(True)
			print "Iniciar gravação..."
		else:
			self.is_recording = False
			
			# Adiciona o novo áudio à tabela de gravações:
			self.records.append(self.recorded_chunks)
			r = len(self.records) - 1
			i = self.listctrl_gravacoes.InsertStringItem(0, time.strftime("%d/%m/%Y %H:%M:%S"))
			self.listctrl_gravacoes.SetStringItem(i, 1, "{} s".format(len(self.recorded_chunks)*PLAYBACK_FRAMES_PER_BUFFER/SAMPLING_RATE))
			self.listctrl_gravacoes.SetStringItem(i, 2, "{} bytes".format(len(self.recorded_chunks)*PLAYBACK_FRAMES_PER_BUFFER*BYTES_PER_FRAME))
			self.listctrl_gravacoes.SetItemData(i, r)
			
			#wf = wave.open("/home/rui/Desktop/temp/teste.wav", 'wb')
			#wf.setnchannels(CHANNELS)
			#wf.setsampwidth(BYTES_PER_FRAME)
			#wf.setframerate(INTENDED_RATE)
			#wf.writeframes(b''.join(self.recorded_chunks))
			#wf.close()
			self.recorded_chunks = []
			self.button_pausar.Enable(False)
			print "\n*******************************\nNovo áudio adicionado\n*******************************"
			self.printList()
			print "*******************************"
	def button_pausar_click_handler(self, event):
		self.is_recording = False
		self.button_gravar.SetToggle(False)
		self.button_pausar.Enable(False)
		print "Pausar gravação..."
	def button_mutar_toggle_handler(self, event):
		if event.GetIsDown():
			self.volume_backup = self.volume
			self.volume = 0.0
		else:
			self.volume = self.volume_backup
	def slider_volume_slide_handler(self, event):
		self.volume = float(self.slider_volume.GetValue())/float(self.slider_volume.GetMax());
	def button_analisar_click_handler(self, event):
		for i in self.getSelectedRecords():
			self.parent.OpenRecord(self.records[self.listctrl_gravacoes.GetItemData(i)])
			self.deleteRecord(i)
	def button_excluir_click_handler(self, event):
		for i in self.getSelectedRecords():
			self.deleteRecord(i)
	def OnClose(self, event):
		self.keep_open = False
		
		# Aguarda a liberação dos recursos de hardware para destruir a janela:
		self.resourceLock.acquire()
		self.resourceLock.release()
		self.Destroy()
	def getSelectedRecords(self):
		indices = []
		lastFound = -1
		while True:
			index = self.listctrl_gravacoes.GetNextSelected(lastFound)
			if index == -1:
				break
			lastFound = index
			indices.append(index)
		return indices
	def deleteRecord(self, i):
		#del self.records[self.listctrl_gravacoes.GetItemData(i)]	# Não pode deletar, para não alterar os indices das gravações subsequentes!
		self.records[self.listctrl_gravacoes.GetItemData(i)] = None
		self.listctrl_gravacoes.DeleteItem(i)
		print "\n###############################\nÁudio removido\n###############################"
		self.printList()
		print "###############################"
	def printList(self):
		cont = self.listctrl_gravacoes.GetItemCount()
		print "============================================"
		print "GRAVAÇÕES ({})".format(cont)
		print "============================================"
		print "i ctrl\ti arr\tBytes ctrl\tBytes arr"
		print "--------------------------------------------"
		for i in range(cont):
			print "{}\t".format(i),
			item = self.listctrl_gravacoes.GetItem(i, 2)
			r = self.listctrl_gravacoes.GetItemData(i)
			print "{}\t".format(r),
			print "{}\t".format(item.GetText()),
			print "{}".format(len(self.records[r])*PLAYBACK_FRAMES_PER_BUFFER*BYTES_PER_FRAME)
		print "--------------------------------------------"
EVT_FRAME_FETCHED = wx.PyEventBinder(wx.NewEventType(), 0)
class FrameFetchedEvent(wx.PyCommandEvent):
	def __init__(self, eventType=EVT_FRAME_FETCHED.evtType[0], id=0):
		wx.PyCommandEvent.__init__(self, eventType, id)
		self.eventLock = None
